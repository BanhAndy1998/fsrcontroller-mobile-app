package com.example.fsrcontroller;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class FSRSettingsActivity extends AppCompatActivity {

    private final int NEW_SETTINGS = 10;
    private final int RESET_PANEL = 99;

    EditText minAct;
    EditText maxAct;
    EditText minThres;
    EditText maxThres;
    Button reset;
    TextView back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fsr_settings);

        minAct = findViewById(R.id.minActSetting);
        maxAct = findViewById(R.id.maxActSetting);
        minThres = findViewById(R.id.minThresSetting);
        maxThres = findViewById(R.id.maxThresSetting);
        reset = findViewById(R.id.resetPanel);
        back = findViewById(R.id.backTextPanel);

        minAct.setText(String.valueOf(getIntent().getExtras().getInt("minAct")));
        maxAct.setText(String.valueOf(getIntent().getExtras().getInt("maxAct")));
        minThres.setText(String.valueOf(getIntent().getExtras().getInt("minThres")));
        maxThres.setText(String.valueOf(getIntent().getExtras().getInt("maxThres")));

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent data = getIntent();
                data.putExtra("minAct",Integer.parseInt(minAct.getText().toString()));
                data.putExtra("maxAct",Integer.parseInt(maxAct.getText().toString()));
                data.putExtra("minThres",Integer.parseInt(minThres.getText().toString()));
                data.putExtra("maxThres",Integer.parseInt(maxThres.getText().toString()));
                setResult(NEW_SETTINGS,data);
                finish();
            }
        });

        reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(RESET_PANEL);
                finish();
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent data = getIntent();
        data.putExtra("minAct",Integer.parseInt(minAct.getText().toString()));
        data.putExtra("maxAct",Integer.parseInt(maxAct.getText().toString()));
        data.putExtra("minThres",Integer.parseInt(minThres.getText().toString()));
        data.putExtra("maxThres",Integer.parseInt(maxThres.getText().toString()));
        setResult(NEW_SETTINGS,data);
        finish();
    }



}