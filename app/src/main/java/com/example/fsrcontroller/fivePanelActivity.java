package com.example.fsrcontroller;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.harrysoft.androidbluetoothserial.BluetoothManager;
import com.harrysoft.androidbluetoothserial.BluetoothSerialDevice;
import com.harrysoft.androidbluetoothserial.SimpleBluetoothDeviceInterface;

import androidx.appcompat.app.AppCompatActivity;

import java.util.Scanner;

import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;


public class FivePanelActivity extends AppCompatActivity {
    private BluetoothManager bluetooth = BluetoothManager.getInstance();
    private SimpleBluetoothDeviceInterface deviceInterface;
    private TextView[] actVals = new TextView[5];
    private TextView[] inputVals = new TextView[5];
    private TextView[] thresVals = new TextView[5];
    private TextView[] arrows = new TextView[5];

    private final int UP_LEFT = 0;
    private final int UP_RIGHT = 1;
    private final int CENTER = 2;
    private final int DOWN_LEFT = 3;
    private final int DOWN_RIGHT = 4;
    private final int NEW_SETTINGS = 10;
    private final int RESET_PANEL = 99;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fivepanel);

        bluetooth.openSerialDevice(getIntent().getStringExtra("btDevice")).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(this::onConnected,this::onError);

        actVals[0] = findViewById(R.id.UpLeftAct);
        actVals[1] = findViewById(R.id.UpRightAct);
        actVals[2] = findViewById(R.id.CenterAct);
        actVals[3] = findViewById(R.id.DownLeftAct);
        actVals[4] = findViewById(R.id.DownRightAct);

        inputVals[0] = findViewById(R.id.UpLeftInput);
        inputVals[1] = findViewById(R.id.UpRightInput);
        inputVals[2] = findViewById(R.id.CenterInput);
        inputVals[3] = findViewById(R.id.DownLeftInput);
        inputVals[4] = findViewById(R.id.DownRightInput);

        thresVals[0] = findViewById(R.id.UpLeftThres);
        thresVals[1] = findViewById(R.id.UpRightThres);
        thresVals[2] = findViewById(R.id.CenterThres);
        thresVals[3] = findViewById(R.id.DownLeftThres);
        thresVals[4] = findViewById(R.id.DownRightThres);

        TextView upLeft = findViewById(R.id.UpLeftDisplay);
        upLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        TextView upRight = findViewById(R.id.UpRightDisplay);
        upRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        TextView center = findViewById(R.id.CenterDisplay);
        center.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        TextView downLeft = findViewById(R.id.DownLeftDisplay);
        downLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        TextView downRight = findViewById(R.id.DownRightDisplay);
        downRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent temp = new Intent(FivePanelActivity.this, FSRSettingsActivity.class);
                Scanner scnr = new Scanner(actVals[4].getText().toString());
                Scanner scnr2 = new Scanner(thresVals[4].getText().toString());
                scnr.useDelimiter("[^\\d]+");
                scnr2.useDelimiter("[^\\d]+");
                temp.putExtra("maxAct",scnr.nextInt());
                temp.putExtra("maxThres",scnr2.nextInt());
                scnr.nextInt();
                scnr2.nextInt();
                temp.putExtra("minAct",scnr.nextInt());
                temp.putExtra("minThres",scnr2.nextInt());
                startActivityForResult(temp,DOWN_RIGHT);
            }
        });

        TextView back = findViewById(R.id.BackText);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        arrows[0] = upLeft;
        arrows[1] = upRight;
        arrows[2] = center;
        arrows[3] = downLeft;
        arrows[4] = downRight;
    }

    private void onConnected(BluetoothSerialDevice currentDevice){
        deviceInterface = currentDevice.toSimpleDeviceInterface();
        deviceInterface.setListeners(this::onMessageReceived, this::onMessageSent, this::onError);
    }

    private void onMessageSent(String message) {
        return;
    }

    private void onMessageReceived(String message) {
        String[] info = message.split(",");
        switch(info[0]){
            case "UL":
                actVals[0].setText("MAX: " + info[1] + "\nCUR: " + info[2] + "\nMIN: " + info[3]);
                inputVals[0].setText(info[4]);
                thresVals[0].setText("MAX: " + info[5] + "\nCUR: " + info[6] + "\nMIN: " + info[7]);
                if(Integer.parseInt(info[4]) >= Integer.parseInt(info[2])) {
                    arrows[0].setTextColor(Color.rgb(255, 0, 0));
                }
                else if (Integer.parseInt(info[4]) < Integer.parseInt(info[6])) {
                    arrows[0].setTextColor(Color.rgb(0, 0, 0));
                }
                break;
            case "UR":
                actVals[1].setText("MAX: " + info[1] + "\nCUR: " + info[2] + "\nMIN: " + info[3]);
                inputVals[1].setText(info[4]);
                thresVals[1].setText("MAX: " + info[5] + "\nCUR: " + info[6] + "\nMIN: " + info[7]);
                if(Integer.parseInt(info[4]) >= Integer.parseInt(info[2])) {
                    arrows[1].setTextColor(Color.rgb(255, 0, 0));
                }
                else if (Integer.parseInt(info[4]) < Integer.parseInt(info[6])) {
                    arrows[1].setTextColor(Color.rgb(0, 0, 0));
                }
                break;
            case "M":
                actVals[2].setText("MAX: " + info[1] + "\nCUR: " + info[2] + "\nMIN: " + info[3]);
                inputVals[2].setText(info[4]);
                thresVals[2].setText("MAX: " + info[5] + "\nCUR: " + info[6] + "\nMIN: " + info[7]);
                if(Integer.parseInt(info[4]) >= Integer.parseInt(info[2])) {
                    arrows[2].setTextColor(Color.rgb(255, 255, 0));
                }
                else if (Integer.parseInt(info[4]) < Integer.parseInt(info[6])) {
                    arrows[2].setTextColor(Color.rgb(0, 0, 0));
                }
                break;
            case "DL":
                actVals[3].setText("MAX: " + info[1] + "\nCUR: " + info[2] + "\nMIN: " + info[3]);
                inputVals[3].setText(info[4]);
                thresVals[3].setText("MAX: " + info[5] + "\nCUR: " + info[6] + "\nMIN: " + info[7]);
                if(Integer.parseInt(info[4]) >= Integer.parseInt(info[2])) {
                    arrows[3].setTextColor(Color.rgb(0, 0, 255));
                }
                else if (Integer.parseInt(info[4]) < Integer.parseInt(info[6])) {
                    arrows[3].setTextColor(Color.rgb(0, 0, 0));
                }
                break;
            case "DR":
                actVals[4].setText("MAX: " + info[1] + "\nCUR: " + info[2] + "\nMIN: " + info[3]);
                inputVals[4].setText(info[4]);
                thresVals[4].setText("MAX: " + info[5] + "\nCUR: " + info[6] + "\nMIN: " + info[7]);
                if(Integer.parseInt(info[4]) >= Integer.parseInt(info[2])) {
                    arrows[4].setTextColor(Color.rgb(255, 0, 255));
                }
                else if (Integer.parseInt(info[4]) < Integer.parseInt(info[6])) {
                    arrows[4].setTextColor(Color.rgb(0, 0, 0));
                }
                break;
        }
    }

    private void onError(Throwable error) {
        return;
    }

    @Override
    protected void onActivityResult(int request, int resultCode, Intent result) {
        super.onActivityResult(request, resultCode, result);
        switch(request) {
            case UP_LEFT:
                break;
            case UP_RIGHT:
                break;
            case CENTER:
                break;
            case DOWN_LEFT:
                break;
            case DOWN_RIGHT:
                switch(resultCode) {
                    case NEW_SETTINGS:
                        break;
                    case RESET_PANEL:
                        break;
                }
                break;
        }
    }
}
