package com.example.fsrcontroller;

import android.Manifest;
import android.annotation.SuppressLint;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.harrysoft.androidbluetoothserial.BluetoothManager;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

public class MainActivity extends AppCompatActivity {

    private static final int REQUEST_BT_PERMISSION = 1;
    private static final int REQUEST_ENABLE_BT = 2;
    private BluetoothManager bluetooth = BluetoothManager.getInstance();
    private BluetoothAdapter ba = BluetoothAdapter.getDefaultAdapter();
    private BluetoothDevice target;
    private TextView text;
    private RelativeLayout layout;
    private Button fourPanelButton;
    private Button fivePanelButton;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        text = findViewById(R.id.ErrorDisplay);
        layout = findViewById(R.id.BluetoothTrueLayout);
        findViewById(R.id.BluetoothTrueLayout).setVisibility(View.GONE);

        if(ba == null) {
            Toast.makeText(getApplicationContext(), "Bluetooth Not Available on this Device", Toast.LENGTH_LONG).show();
            finish();
        }
        else if (ActivityCompat.checkSelfPermission(this, Manifest.permission.BLUETOOTH_ADMIN) != PackageManager.PERMISSION_GRANTED || ActivityCompat.checkSelfPermission(this,Manifest.permission.BLUETOOTH) != PackageManager.PERMISSION_GRANTED || ActivityCompat.checkSelfPermission(this,Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[] {Manifest.permission.BLUETOOTH,Manifest.permission.BLUETOOTH_ADMIN,Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_BT_PERMISSION);
        }

        if (!ba.isEnabled()) {
            Intent enableBluetoothIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBluetoothIntent, REQUEST_ENABLE_BT);
        }
        else {
            text.setText(getResources().getString(R.string.errorBTName));
            for (BluetoothDevice d : ba.getBondedDevices()) {
                if (d.getAlias().compareTo("pad") == 0){
                    text.setVisibility(View.GONE);
                    target = d;
                    layout.setVisibility(View.VISIBLE);
                    fourPanelButton = findViewById(R.id.FourPanelButton);
                    fivePanelButton = findViewById(R.id.FivePanelButton);
                }
            }
        }

        if(fourPanelButton != null) {
            fourPanelButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(getApplicationContext(), "Currently disabled.", Toast.LENGTH_LONG).show();
                }
            });

            fivePanelButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent temp = new Intent(MainActivity.this, FivePanelActivity.class);
                    temp.putExtra("btDevice", target.getAddress());
                    startActivity(temp);
                }
            });
        }


    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onActivityResult(int request, int resultCode, Intent result) {
        super.onActivityResult(request, resultCode, result);
        if (request == REQUEST_BT_PERMISSION) {
            if(checkSelfPermission(Manifest.permission.BLUETOOTH_ADMIN) == PackageManager.PERMISSION_GRANTED && checkSelfPermission(Manifest.permission.BLUETOOTH_ADMIN) == PackageManager.PERMISSION_GRANTED && checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(getApplicationContext(), "Bluetooth Permissions Granted", Toast.LENGTH_LONG).show();
            }
            else {
                Toast.makeText(getApplicationContext(), "Bluetooth Permissions Request Denied, Exiting...", Toast.LENGTH_LONG).show();
                finish();
            }
        }
        if (request == REQUEST_ENABLE_BT) {
            if(resultCode == RESULT_OK) {
                Toast.makeText(getApplicationContext(), "Bluetooth Enabled", Toast.LENGTH_LONG).show();
            }
            if(resultCode == RESULT_CANCELED) {
                Toast.makeText(getApplicationContext(), "Bluetooth Enable Request Denied, Exiting...", Toast.LENGTH_LONG).show();
                finish();
            }
        }
    }
}