# FSRController Android APP (For use with 4-5 Panel Dance Pad FSR project)
**Created by Andy Banh**

This is an Android mobile app created function with my 4-5 Panel Dance Pad Arduino program.
If your phone does not support bluetooth then this app is not compatible with your mobile device.
An HC-05 Bluetooth Module for Arduino is also required for this app to function properly. 

To see how to wire your HC-05 bluetooth module to your Arduino, please refer to this page for instructions.
https://www.instructables.com/Remotely-Control-LED-using-HC-05-Bluetooth-Arduino/

How To Use
-------------------

When you have paired your HC-05 bluetooth module to your Android mobile device. **RENAME** the device name to simply "pad" or else this app will not function or will give you an error. Please note that your mobile device may not display that it is connected to your HC-05 bluetooth module, however as long as you are within range of it and your bluetooth is enabled on your mobile device, this app will function properly.

If the device gives an error because you forgot to enable bluetooth or your HC-05 bluetooth device is not named "pad" then please restart the app once you have corrected the issue.

Currently only settings for five panel dance pads are working. Settings for four panel pads will be implemented in the future.

The panel information activity will not display any information from your Arduino if debug mode is not enabled. You can enable debug mode either by bridging the 9 pin to ground and rebooting your Arduino, or by enabling it using the debug mode toggle that is on this activity (To be implemented). Auto mode functionality can also be enabled on this activity which will cause the panels to adjust their settings if the Arduino determines that a panel may be stuck or was close to firing (To be implemented). The activity displays relevant information from your pad such as max, current, and min activation values and deactivation values. It also displays the current input force reading from each of the panels. 

(This is not implemented yet) You can change some settings on each panel by tapping on the icon of the panel whose settings you wish to change. This FSR settings activity will allow you to change the min and max values that the activation and deactivation values can be set to if auto mode is enabled. If auto mode is not enabled, then only the values under the max column will be relevant and these will change the static activation and deactivation values to a new value of your choosing. The min values will do nothing in this case. Once you are satisfied with your settings, simply hit back or use android's back button to save the settings. The reset button will set your Arduino back to the default settings from the 4-5 Panel Dance Pad Project's code.
